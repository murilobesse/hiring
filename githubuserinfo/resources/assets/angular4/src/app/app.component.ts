import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { JsonPipe } from '@angular/common';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data = {};
  form;
  ngOnInit(){
    this.form = new FormGroup({
        username: new FormControl("", Validators.required)
    });
  }

  constructor(private http: Http){
  }
  onSubmit = function(user) {
    this.http.get('http://localhost:8080/show?username=' + user.username)
    .subscribe(
    (res Response)=>{
    this.data = res.json();
    })
  }

}
