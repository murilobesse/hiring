1 - Setup Laravel: https://kyleferg.com/laravel-development-with-docker/#laravel-setup

2 - Setup Docker: https://docs.docker.com/

3 - Make sure both is running.

4 - In the project root folder add docker-compose.yml file:
    version: '2'
    services:
        web:
            build:
                context: ./
                dockerfile: web.docker
            volumes:
                - ./:/var/www
            ports:
                - "8080:80"
            links:
                - app
        app:
            build:
                context: ./
                dockerfile: app.docker
            volumes:
                - ./:/var/www

5 - add app.docker:
    FROM php:7-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev mysql-client \
    && docker-php-ext-install mcrypt pdo_mysql

WORKDIR /var/www
6 - add web.docker:
    FROM nginx:1.10

ADD ./vhost.conf /etc/nginx/conf.d/default.conf
WORKDIR /var/www
7 - add vhost.conf:
    server {
        listen 80;
        index index.php index.html;
        root /var/www/public;

    location / {
        try_files $uri /index.php?$args;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass app:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
8 - Setup angular4: https://angular.io/guide/quickstart

9 - create projet (command: ng new angular4) in resources/assets

10 - run "ng build --output-path=./../../../public/js --watch" inside resources/assets/angular4

11 - change file resources/view/welcome.blade.php to:
    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Github User Info</title>
      <base href="/">

      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      {{ Html::style('css/materialize.css') }}
    </head>
    <body>
      <app-root></app-root>
    <script type="text/javascript" src="js/inline.bundle.js"></script>
    <script type="text/javascript" src="js/polyfills.bundle.js"></script>
    <script type="text/javascript" src="js/styles.bundle.js"></script>
    <script type="text/javascript" src="js/vendor.bundle.js"></script>
    <script type="text/javascript" src="js/main.bundle.js"></script>
    </body>
    </html>
    
12 - Once you run the command $ docker-compose up -d it should be runnin on http://localhost:8080
